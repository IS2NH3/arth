using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;
using MesozoicConsole;

namespace MesozoicTest
{
    public class LaboratoryTest
    {
    [Fact]
        public void TestLaboratoryConstructor()
        {
            Laboratory Labo = new Laboratory("Vidal");

            Assert.Equal("Vidal", Labo.getName());
        }

    [Fact]
        public void LaboCreateDinosaurTest()
        {
            Laboratory Labo = new Laboratory("Vidal");
            Dinosaur Jean = Labo.createDinosaur("Jean", "Steack aux ors Russes");
            Assert.Equal("Jean", Jean.getName());

        }
    }
}