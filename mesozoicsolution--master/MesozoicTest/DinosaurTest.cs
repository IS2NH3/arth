using System;
using Xunit;
using Mesozoic;
using System.Collections.Generic;

namespace MesozoicTest
{
    public class DinosaurTest
    {

        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Grrr", louis.roar());
        }

   
        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.SayHello());
        }

        [Fact]
           public void TestSetDino()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            louis.setName("Jean");
            louis.setSpecie("Velociraptor");
            louis.setAge(34);
            Assert.Equal("Jean", louis.getName());
            Assert.Equal("Velociraptor", louis.getSpecie());
            Assert.Equal(34, louis.getAge());
        }
        [Fact]
        public void TestHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur ("Nessie", "Diplodocus", 11);
            Assert.Equal("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

         [Fact]
        public void testHordeConstructor()
        {
            Horde AS = new Horde();
            Assert.IsType<List<Dinosaur>>(AS.getList());      
        }
        [Fact]
        public void testgetSize()
        {
            Horde AS = new Horde();
            Assert.Equal(0,AS.getSize());
        }

        [Fact]
        public void testAddDino()
        {
            Dinosaur Louis = new Dinosaur("Louis", "Stegausaurus", 8);
            Horde AS = new Horde();
            AS.AddDino(Louis);
            Assert.Equal("Louis",AS.getList()[0].getName());
        }
        [Fact]
        public void testGetDino()
        {
            Dinosaur Louis = new Dinosaur ("Louis","Stegausaurus", 8);
            Horde teamLouis = new Horde();
            teamLouis.AddDino(Louis);
            Assert.Equal("Louis", teamLouis.getDino(0).getName());
        }
    }
}