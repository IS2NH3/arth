using System;
using System.Collections.Generic;

namespace Mesozoic
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs ;
        public Horde()
        {
            this.dinosaurs = new List<Dinosaur>();
        }
        public List<Dinosaur> getList()
        {
            return this.dinosaurs;
        }
        public int getSize()
        {
            return this.dinosaurs.Count;
        }
        public void AddDino(Dinosaur dino)
        {
            this.dinosaurs.Add(dino);
        }
        public Dinosaur getDino(int index)
        {
            return this.dinosaurs[index];
        }
        
        public void DelDino(Dinosaur dino )
        {
            this.dinosaurs.Remove(dino);
        }

        public void hordePresentation()
        {
            foreach (Dinosaur dino in this.dinosaurs)
            {
                Console.WriteLine(dino.SayHello());
            }
        }
    }
}
