using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        protected string name;
        protected static string specie;
        protected int age;


        public Dinosaur(string name,string specie, int age)
        {
            this.name=name;
            Dinosaur.specie=specie;
            this.age=age;
        }

        public string SayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
            
        }

        public string roar()
        {
            return "Grrr";
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string name)
        {
            this.name=name;
        }

        public string getSpecie()
        {
            return Dinosaur.specie;
        }

        public void setSpecie(string specie)
        {
            Dinosaur.specie=specie;
        }

        public int getAge()
        {
            return this.age;
        }

        public void setAge(int age)
        {
            this.age=age;
        }

        public string hug(Dinosaur dinosaur)
        {
            string hug=string.Format("Je suis {0} et je fais un calin à Nessie.", this.name, dinosaur.getName());
            return hug;
        }
    }
}