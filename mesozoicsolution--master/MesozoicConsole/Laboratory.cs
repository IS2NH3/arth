using System;
using Mesozoic;


namespace MesozoicConsole
{
    public class Laboratory
    {
        protected string name;

        public Laboratory(string name)
        {
            this.name=name;
        }

        public string getName()
        {
            return this.name;
        }

        public Dinosaur createDinosaur(string name, string specie)
        {
            Dinosaur dino= new Dinosaur(name, specie, 12);
            return dino;
        }
    }
}