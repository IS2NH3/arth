﻿using System;
using Mesozoic;
using System.Collections.Generic;

namespace MesozoicConsole
{
    class Program
    {
      static void Main(string[] args)
        {
           /* Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur samy  = new Dinosaur("Samy", "Tortusaurus", 30);
            Dinosaur arthur = new Dinosaur("Arthur", "Arthusaurus", 23);

            Horde LesZamis = new Horde();

            LesZamis.AddDino(louis); //Append dinosaur reference to end of list
            LesZamis.AddDino(nessie);
            LesZamis.AddDino(samy);
            LesZamis.AddDino(arthur);

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();
            LesZamis.getList().RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();

            LesZamis.DelDino(louis);

            Console.WriteLine(LesZamis.getSize());
            //Iterate over our list
            LesZamis.hordePresentation();
*/

            Dinosaur henry = new Dinosaur("Henry","Diplodocus",22);
            Console.WriteLine(henry.SayHello());
            Dinosaur louis = new Dinosaur("Louis","Stegausaurus",11);
            Console.WriteLine(louis.SayHello());
            Console.WriteLine(henry.SayHello());

            Laboratory Ingen = new Laboratory("ZI LABO");
            Dinosaur Jean = Ingen.createDinosaur("Jean", "Steack aux ors Russes");
            Console.WriteLine(Jean.SayHello());
            Dinosaur Paul = Ingen.createDinosaur("Paul", "Dominocus");
            Console.WriteLine(Paul.SayHello());
            Dinosaur Deux = Ingen.createDinosaur("Deux", "JurassicPape");
            Console.WriteLine(Deux.SayHello());
        }

    }
}
